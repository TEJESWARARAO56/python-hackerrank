#zipped
# Enter your code here. Read input from STDIN. Print output to STDOUT
m,n=[int(x) for x in input().split()]
c=[[float(x) for x in input().split()]]
for i in range(n-1):
    d=[float(x) for x in input().split()]
    c.append(d)
c=list(zip(*c))
for i in c:
    print(sum(i)/n)
