# set discard() remove() pop()
n = int(input())
s = set(map(int, input().split()))
no_of_operations=int(input())
for i in range(no_of_operations):
    operation=input().split()
    if operation[0][0]=="d":
        s.discard(int(operation[1]))
    elif operation[0][0]=="r":
        s.remove(int(operation[1]))
    else:
        s.pop()
print(sum((s)))
