#validating email addresses
import re
def fun(s):
    s=s.split("@")
    y=s[1].split(".")
    try:
        isvalidun=re.search(r'[0-9a-zA-Z-_]*',s[0])
        if len(isvalidun.group())!=len(s[0]):
            return False
        isvalidwebname=re.search(r'[0-9a-zA-Z]*',s[1])
        if len(isvalidwebname.group())!=len(y[0]):
            return False
        isvalid=re.search(r'[a-zA-Z]*{0-3}',y[1])
        
        
        return True
        
    except:
        return False
    # return True if s is a valid email, else return False

def filter_mail(emails):
    return list(filter(fun, emails))

if __name__ == '__main__':
    n = int(input())
    emails = []
    for _ in range(n):
        emails.append(input())

filtered_emails = filter_mail(emails)
filtered_emails.sort()
print(filtered_emails)
