#iterables and itertools
# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import combinations
n=input()
string="".join(input().split())
length=int(input())
combos=tuple(combinations(string,length))
total =len(combos)
fav_chances=0
for i in combos:
    if "a" in i:
        fav_chances+=1
print(fav_chances/total)
