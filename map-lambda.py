#map and lambda function
cube = lambda x:x**3 # complete the lambda function

def fibonacci(n):
    # return a list of fibonacci numbers
    fibonacci_s=[0,1]
    if n<=2:
        return list(range(n))
    else:
        for i in range(n-2):
            fibonacci_s.append(fibonacci_s[-2]+fibonacci_s[-1])
        return fibonacci_s

if __name__ == '__main__':
    n = int(input())
    print(list(map(cube, fibonacci(n))))
