# Enter your code here. Read input from STDIN. Print output to STDOUT
a,b=list(map(int,input().split()))
for i in range(1,a+1):
    if i==(a//2)+1:
        print("WELCOME".center(b,"-"))
    elif i<=a//2:
        reqstr=""
        for j in range(2*i-1):
            reqstr+=".|."
        print(reqstr.center(b,"-"))
    else:
        reqstr=""
        for j in range((a-i)*2+1):
            reqstr+=".|."
        print(reqstr.center(b,"-"))
