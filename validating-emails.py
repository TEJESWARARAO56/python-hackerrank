#Validating Parsing email addresses
# Enter your code here. Read input from STDIN. Print output to STDOUT
import re
import email.utils
n=int(input())
for i in range(n):
    name,mail=input().split()
    if re.search(r"<[a-zA-Z][a-zA-Z0-9\-\.\_]+@[a-zA-Z]+\.[a-zA-Z]{1,3}>",mail):
        print(name+" "+mail)
