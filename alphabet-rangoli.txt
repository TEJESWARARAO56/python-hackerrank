#Alphabet Rangoli
def print_rangoli(size):
    # your code goes here
    strobj={}
    highest_unicode=96+size
    for i in range(1,2*size):
        currli=[]
        if i>size:
            i=size*2-i
        highest=highest_unicode
        for j in range(i):
            currli.append(chr(highest))
            highest-=1
        currli=currli+sorted(currli)[1:]
        reqstr="-".join(currli)
        print(reqstr.center(4*(size-1)+1,"-"))
    return None
if __name__ == '__main__':
    n = int(input())
    print_rangoli(n)
