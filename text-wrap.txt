#Text wrap
import textwrap

def wrap(string, max_width):
    reqstr=""
    for i in range(len(string)//max_width):
        reqstr+=string[max_width*i:max_width*(i+1)]+"\n"
    return reqstr+string[-1*(len(string)%max_width):]

if __name__ == '__main__':
    string, max_width = input(), int(input())
    result = wrap(string, max_width)
    print(result)
