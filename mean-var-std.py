#mean var std
import numpy
n,m=list(map(int,input().split()))
list_s=[]
for i in range(n):
     list_s.append([int(x) for x in input().split()])
# print(list_s)

my_array=numpy.array(list_s)
print (numpy.mean(my_array, axis = 1))  
print (numpy.var(my_array, axis = 0))
print(numpy.std(my_array, axis = None))
