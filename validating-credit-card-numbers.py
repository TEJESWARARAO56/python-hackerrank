#Validating credit card numbers
# Enter your code here. Read input from STDIN. Print output to STDOUT
import re
n=int(input())
for i in range(n):
    credit_card_num=input()
    if len(re.findall(r"[0-9]{1}",credit_card_num))==16:
        pass
    else:
        print("Invalid")
        continue
    res=re.search(r"(^[4-6][0-9]{3}-?\d{4}-?\d{4}-?\d{4})",credit_card_num)
    if res==None:
        print("Invalid")
        continue
    else:
        creditwithoutspaces="".join(credit_card_num.split("-"))
        s="Valid"
        for j in range(len(creditwithoutspaces)-3):
            if len(set(creditwithoutspaces[j:j+4]))==1:
                s="Invalid"
                break
    print(s)
