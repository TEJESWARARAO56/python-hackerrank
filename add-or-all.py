#Any or all
# Enter your code here. Read input from STDIN. Print output to STDOUT
n=input()
i=list(map(int,input().split()))

if all([x>0 for x in i]):
    booli=any([str(x)==str(x)[::-1] for x in i])
    print(booli)
else:
    print("False")
