#set mutations
# Enter your code here. Read input from STDIN. Print output to STDOUT
n=int(input())
set_a=set(map(int,input().split()))
for i in range(int(input())):
    operation=input()
    new_set=set(map(int,input().split()))
    if operation[0]=="d":
        set_a.difference_update(new_set)
    elif operation[0]=="s":
        set_a.symmetric_difference_update(new_set)
    elif operation[0]=="u":
        set_a.update(new_set)
    elif operation[0]=="i":
        set_a.intersection_update(new_set)
print(sum(list(set_a)))
